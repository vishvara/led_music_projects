// NeoPixel Ring simple sketch (c) 2013 Shae Erisson
// released under the GPLv3 license to match the rest of the AdaFruit NeoPixel library

#include <Adafruit_NeoPixel.h>

#ifdef __AVR__
  #include <avr/power.h>
#endif

#define TRIG_MIC 0
#define TRIG_SPEC 1
#define TRIGGER TRIG_SPEC

#if(TRIGGER==TRIG_SPEC)
//Spectrum Shield Defines and Vars
#define STROBE 4
#define RESET 5
#define DC_One A0
#define DC_Two A1
int freq_amp;
uint16_t fA[7];
uint16_t fB[7];

#define AMPTH_1 500
#define AMPTH_2 900
#define AMPTH_3 900

#elif(TRIGGER==TRIG_MIC)
#define MIC       6
#endif

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1
#define ADDR      2

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS      20

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUMPIXELS, ADDR, NEO_GRB + NEO_KHZ800);

uint32_t s_red =  strip.Color(255,0,0);
uint32_t s_blue=  strip.Color(0,0,255);
uint32_t s_green= strip.Color(0,255,0);

uint16_t delayval = 10;

uint32_t TimerA = 0;
uint32_t prevMillis=0;
uint32_t TimerB = 0;

int RoutineSwitch = random(0,6);

//Vars for the "shots fired" routine
int shotsDir=1;
int shotsLoopCount=0;
uint16_t shotsIndex=0;

//Vars for the legs animation
enum LegPatterns {
  Horiz = 0,
  InOut = 1,
  OutIn = 2
};

void setup() {
  // put your setup code here, to run once:
  // This is for Trinket 5V 16MHz, you can remove these three lines if you are not using a Trinket
#if defined (__AVR_ATtiny85__)
  if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
#endif
  // End of trinket special code

  strip.begin(); // This initializes the NeoPixel library.
#if(TRIGGER==TRIG_MIC)
  pinMode(MIC,INPUT);
#elif(TRIGGER==TRIG_SPEC)
  //Set spectrum Shield pin configurations
  pinMode(STROBE, OUTPUT);
  pinMode(RESET, OUTPUT);
  pinMode(DC_One, INPUT);
  pinMode(DC_Two, INPUT);
  digitalWrite(STROBE, HIGH);
  digitalWrite(RESET, HIGH);

  //Initialize Spectrum Analyzers
  digitalWrite(STROBE, LOW);
  delay(1);
  digitalWrite(RESET, HIGH);
  delay(1);
  digitalWrite(STROBE, HIGH);
  delay(1);
  digitalWrite(STROBE, LOW);
  delay(1);
  digitalWrite(RESET, LOW);
#endif
}

void loop() {
  static uint32_t QCtr;
  if(shotsIndex==NUMPIXELS) shotsIndex=0;

  if(millis() - TimerB > 7000)
  {
    TimerB = millis();
    RoutineSwitch=random(0,4);
  }


  #if 0

  #if(TRIGGER==TRIG_SPEC)
  if(micros() - TimerA > 50000)
  {
    TimerA = micros();
    Read_Frequencies();
  }
#endif
#if 0
  if(fA[2]>400)
#elif(TRIGGER==TRIG_MIC)

  if(digitalRead(MIC)==1)
  
#endif
  {
#endif
    switch(RoutineSwitch)
    {
      case 0:
      chaseStrobeRev(genRandCol(), genRandCol());
      break;
      case 1:
      strobeAll(genRandCol(),100);
      break;
      case 2:
      TwinkleTwinkle(15,25);
      break;
      case 3:
      chaseStrobeF(genRandCol(), genRandCol(), 50);
      break;
      case 4:
      FadeInOut(genRandCol(), 50);
      }
      #if 0
  }

  else
  {
    stripOff();
  }
#endif
}


uint32_t genRandCol()
{
  return strip.Color(random(0,75),random(0,75),random(0,75));
}

void stripOff()
{
  int i;
  for(i=0;i<NUMPIXELS;i++) strip.setPixelColor(i,0);
  strip.show();
}

void stripColor(uint32_t c)
{
  int i;
  for(i=0;i<NUMPIXELS;i++)
  {
    strip.setPixelColor(i,c);
  }
  strip.show();
}


/*****************Effect Routines*****************/
/************************************************/
void FadeInOut(uint32_t c, uint8_t wait)
{
  int r = c >> 16 & 0xff;
  int g = c >> 8 & 0xff;
  int b = c & 0xff;
  uint32_t c2;
  int waithalf = wait/2;
  for(int i=0;i<wait;i++)
  {
    c2 = strip.Color(i*(r/wait),i*(g/wait),i*(b/wait));
    stripColor(c2);;
    delay(50);
  }
  for(int i=0;i<wait;i++)
  {
    c2 = strip.Color((wait - i)*(r/wait),(wait-i)*(g/wait),(wait-i)*(b/wait));
    stripColor(c2);;
    delay(50);
  }
}
void WAORainbow(uint8_t wait)
{
  rainbow(wait, 150);
}
void strobeAll(uint32_t c, uint8_t wait)
{  
  stripColor(c);
 
  delay(wait);
  stripOff();
 
}

void SRStrobe(uint32_t c1, uint32_t c2, uint32_t c3)
{
    int i;
    
    for(i=0; i<NUMPIXELS; i+=2) strip.setPixelColor(i, c1);
    strip.show();
    delay(30);
    for(i=0;i<NUMPIXELS;i+=2)
    {
      strip.setPixelColor(i,0);
      strip.setPixelColor(i+1,c2);
    }
    strip.show();
    delay(30);
}


void shotsFired(uint32_t c, uint8_t skip, uint16_t numLights, uint8_t wait)
{
  int i, j;
  uint32_t curState[NUMPIXELS];

 
  stripColor(0);

    for(i=shotsLoopCount;i<shotsLoopCount+numLights;i++)
    { 
      for(j=0;j<NUMPIXELS;j+=50)
      {
        strip.setPixelColor(i-j,c);
      }
    }

    for(i=NUMPIXELS-shotsLoopCount;i>(NUMPIXELS-shotsLoopCount-numLights);i--)
    {
      for(j=0;j<NUMPIXELS;j+=50)
      {
        strip.setPixelColor(i+j,c);
      }
    }

  shotsLoopCount+=skip;
  if(shotsLoopCount == NUMPIXELS+250)
  { 
    shotsLoopCount=0;
    if(shotsDir==1) shotsDir=0;
    else shotsDir=1;
  }

  strip.show();
  delay(wait);
  stripOff();
  delay(wait);
  
}


void chaseStrobeF(uint32_t c1, uint32_t c2, uint8_t wait)
{
  theaterChase(genRandCol(),4,wait);    
}

void chaseStrobeRev(uint32_t fCol, uint32_t rCol)
{  
    static int chaseDir;
    if(chaseDir==0)
    {
      theaterChase(genRandCol(),4,8);    
//      chaseDir=random(0,2);
      chaseDir=1;
    }
    else
    {
      theaterChaseReverse(genRandCol(),4,8);
//      chaseDir=random(0,2);
      chaseDir=0;
    }
}


void Legs(uint32_t col, uint8_t wait)
{
  static int whichstep;
  int i;
  if(whichstep==0)
  {
    for(i=0;i<NUMPIXELS/4;i++)
    {
      strip.setPixelColor(NUMPIXELS-i,0);
      strip.setPixelColor(i,col);
    }
  }
  else if(whichstep==1)
  {
    for(i=NUMPIXELS/4;i<NUMPIXELS/2;i++)
    {
      strip.setPixelColor(i,col);
      strip.setPixelColor(i-NUMPIXELS/4,0);
    }
  }
  else if(whichstep==2)
  {
    for(i=NUMPIXELS/2;i<NUMPIXELS/4*3;i++)
    {
      strip.setPixelColor(i,col);
      strip.setPixelColor(i-NUMPIXELS/4,0);
    }
  }
  else if(whichstep==3)
  {
    for(i=NUMPIXELS/4*3;i<NUMPIXELS;i++)
    {
      strip.setPixelColor(i,col);
      strip.setPixelColor(i-NUMPIXELS/4,0);
    }
  }
  strip.show();
  whichstep++;
  if(whichstep>3)
  {
    whichstep=0;
  }
  delay(wait);
}

void Legs2(LegPatterns pattern, uint32_t col, uint8_t wait)
{
  static int whichstep;
  int i;
  uint16_t istart,istop;
  switch(pattern)
  {
    case InOut:
      if(whichstep==0)
      {
        istart=0;
        istop=NUMPIXELS/4;
      }
      else if(whichstep==1)
      {
        istart=NUMPIXELS/4;
        istop=NUMPIXELS/4;
      }
      for(i=istart;i<istop;i++)
      {
        strip.setPixelColor(i,col);
        strip.setPixelColor(NUMPIXELS - i, col);
      }
      strip.show();
      delay(wait);
      whichstep++;
      if(whichstep==2) whichstep=0;
    break;
    case Horiz:
    break;
  }
}

void WAOPulse(uint32_t col, uint8_t wait)
{
  int i;
  for(i=150;i<NUMPIXELS;i++)
  {
    strip.setPixelColor(i,col);
  }
  strip.show();
  delay(wait);

  stripOff();
  delay(wait);
}

void TwinkleTwinkle(uint32_t wait,uint8_t spacing)
{
  int i;
  for(i=0;i<NUMPIXELS;i+=spacing)
  {
    strip.setPixelColor(random(i,i+spacing),strip.Color(180,180,180));
  }
  strip.show();
  delay(wait);

  stripOff();
  delay(wait);
}

/********************Routine Support Routines***********/
/******************************************************/
//Theatre-style crawling lights.
void theaterChase(uint32_t c, uint8_t num, uint8_t wait) {
  for (int j=0; j<2; j++) {  //do 10 cycles of chasing
    for (int q=0; q < num; q++) {
      for (uint16_t i=0; i < strip.numPixels(); i=i+num) {
        strip.setPixelColor(i+q, c);    //turn every third pixel on
      }
      strip.show();

      delay(wait);

      for (uint16_t i=0; i < strip.numPixels(); i=i+num) {
        strip.setPixelColor(i+q, 0);        //turn every third pixel off
      }
    }
  }
}

//Theatre-style crawling lights.
void theaterChaseReverse(uint32_t c, uint8_t num, uint8_t wait) {
  for (int j=2; j>0; j--) {  //do 10 cycles of chasing
    for (int q=num; q > 0; q--) {
      for (uint16_t i=NUMPIXELS; i > 0; i=i-num) {
        strip.setPixelColor(i+q, c);    //turn every third pixel on
      }
      strip.show();

      delay(wait);

      for (uint16_t i=NUMPIXELS; i > 0; i=i-num) {
        strip.setPixelColor(i+q, 0);        //turn every third pixel off
      }
    }
  }
}

//Theatre-style crawling lights with rainbow effect
void theaterChaseRainbow(uint8_t wait) {
  for (int j=0; j < 2; j++) {     // cycle all 256 colors in the wheel
    for (int q=0; q < 3; q++) {
      for (uint16_t i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(i+q, Wheel( (i+j) % 255));    //turn every third pixel on
      }
      strip.show();

      delay(wait);

      for (uint16_t i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(i+q, 0);        //turn every third pixel off
      }
    }
  }
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}

void rainbow(uint8_t wait, uint16_t startPix) {
  uint16_t i, j;

  for(j=0; j<256; j++) {
    for(i=startPix; i<strip.numPixels(); i++) {
      strip.setPixelColor(i, Wheel((i+j) & 255));
    }
    strip.show();
    delay(wait);
  }
}


uint32_t colorFreq()
{
 uint8_t r,g,b;
// uint32_t out;
 r = map((fA[0]+fA[1]),0,2048,0,180);
 g = map((fA[2]+fA[3]),0,2048,0,180);
 b = map((fA[4]+fA[5]),0,2048,0,180);
 return strip.Color(r,g,b);
}

/*******************Pull frquencies from Spectrum Shield********************/
void Read_Frequencies()
{
  //Read frequencies for each band
  for (freq_amp = 0; freq_amp < 7; freq_amp++)
  {
    fA[freq_amp] = analogRead(DC_One);
    fB[freq_amp] = analogRead(DC_Two);
    digitalWrite(STROBE, HIGH);
    digitalWrite(STROBE, LOW);
  }
}




